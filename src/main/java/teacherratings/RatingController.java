package teacherratings;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rating")
public class RatingController {

    @GetMapping("/{teacherId}")
    public Rating getRatingByBookId(
            @PathVariable("teacherId") String teacherId) {

        return new Rating(teacherId, 5);
    }
}
